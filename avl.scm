(define empty-tree '())

(define
	(tree-null? tree)
	(eq? tree empty-tree))

(define
	(make-avl-node data left right height)
	(list data left right height))

(define
	(make-tree-node data left right)
	(list data left right))

(define
	(get-tree-data tree-node)
	(car tree-node))

(define
	(get-tree-left tree-node)
	(cadr tree-node))

(define
	(get-tree-right tree-node)
	(caddr tree-node))

(define
	(get-tree-height tree-node)
	(if
		(or (tree-null? tree-node) (null? (cdddr tree-node)))
		-1
		(cadddr tree-node)))

(define
	(set-avl-data tree-node data)
	(make-avl-node
		data
		(get-tree-left tree-node)
		(get-tree-right tree-node)
		(get-tree-height tree-node)))

(define
	(set-tree-data tree-node data)
	(make-tree-node
		data
		(get-tree-left tree-node)
		(get-tree-right tree-node)))
		
(define
	(set-avl-left tree-node left)
	(make-avl-node
		(get-tree-data tree-node)
		left
		(get-tree-right tree-node)
		(get-tree-height tree-node)))

(define
	(set-tree-left tree-node left)
	(make-tree-node
		(get-tree-data tree-node)
		left
		(get-tree-right tree-node)))

(define
	(set-avl-right tree-node right)
	(make-avl-node
		(get-tree-data tree-node)
		(get-tree-left tree-node)
		right
		(get-tree-height tree-node)))

(define
	(set-tree-right tree-node right)
	(make-tree-node
		(get-tree-data tree-node)
		(get-tree-left tree-node)
		right))
		
(define
	(set-avl-height tree-node height)
	(if
		(tree-null? tree-node)
		empty-tree
		(make-avl-node
			(get-tree-data tree-node)
			(get-tree-left tree-node)
			(get-tree-right tree-node)
			height)))

(define
	(update-height tree)
	(set-avl-height
		tree
		(+
			1
			(get-tree-height
				(if
					(> (get-tree-height (get-tree-left tree)) (get-tree-height (get-tree-right tree)))
					(get-tree-left tree)
					(get-tree-right tree))))))

(define
	(left-rotate tree)
	(cond
		((tree-null? tree) empty-tree)
		((tree-null? (get-tree-right tree)) #f)
		(else
			(update-height
				(set-avl-right
					(set-avl-left
						(get-tree-right tree)
						(update-height
							(set-avl-left
								(set-avl-right
									tree
									(get-tree-left
										(get-tree-right tree)))
								(get-tree-left tree))))
					(get-tree-right (get-tree-right tree)))))))

(define
	(right-rotate tree)
	(cond
		((tree-null? tree) empty-tree)
		((tree-null? (get-tree-left tree)) #f)
		(else
			(update-height
				(set-avl-left
					(set-avl-right
						(get-tree-left tree)
						(update-height
							(set-avl-right
								(set-avl-left
									tree
									(get-tree-right
										(get-tree-left tree)))
								(get-tree-right tree))))
					(get-tree-left (get-tree-left tree)))))))

; (comp x y) if x < y
; (equal x y) if x = y
(define
	(search-tree tree target comp equal)
	(cond
		((tree-null? tree) #f)
		((equal (get-tree-data tree) target) (get-tree-data tree))
		((comp target (get-tree-data tree)) (search-tree (get-tree-left tree) target comp equal))
		(else (search-tree (get-tree-right tree) target comp equal))))

(define
	(balance-avl tree)
	(if
		(<
			(abs
				(-
					(get-tree-height
						(get-tree-left tree))
					(get-tree-height
						(get-tree-right tree))))
			2)
		tree
		(if
			(<
				(get-tree-height
					(get-tree-left tree))
				(get-tree-height
					(get-tree-right tree)))
			(if
				(<
					(get-tree-height
						(get-tree-left
							(get-tree-right tree)))
					(get-tree-height
						(get-tree-right
							(get-tree-right tree))))
				(left-rotate tree)
				(left-rotate
					(set-avl-right
						(right-rotate
							(get-tree-right tree)))))
			(if
				(<
					(get-tree-height
						(get-tree-right
							(get-tree-left tree)))
					(get-tree-height
						(get-tree-left
							(get-tree-left tree))))
				(right-rotate tree)
				(right-rotate
					(set-avl-left
						(left-rotate
							(get-tree-left tree))))))))

(define
	(insert-tree tree target comp)
	(cond
		((tree-null? tree) (make-tree-node target empty-tree empty-tree))
		((comp target (get-tree-data tree))
			(set-tree-left
				tree
				(insert-tree (get-tree-left tree) target comp)))
		(else
			(set-tree-right
				tree
				(insert-tree (get-tree-right tree) target comp)))))

(define
	(insert-subtree tree target comp)
	(cond
		((tree-null? tree) target)
		((comp (get-tree-data target) (get-tree-data tree))
			(set-tree-left
				tree
				(insert-tree (get-tree-left tree) target comp)))
		(else
			(set-tree-right
				tree
				(insert-tree (get-tree-right tree) target comp)))))

(define
	(delete-tree tree target comp equal)
	(cond
		((tree-null? tree) empty-tree)
		((equal target (get-tree-data tree))
			(if
				(tree-null? (get-tree-right tree))
				(get-tree-left tree)
				(make-tree-node
					(get-tree-data
						(get-tree-right tree))
					(insert-subtree
						(get-tree-left
							(get-tree-right tree))
						(get-tree-left tree)
						comp)
					(get-tree-right
						(get-tree-right tree)))))
		((comp target (get-tree-data tree))
			(set-tree-left
				tree
				(delete-tree (get-tree-left tree) target comp equal)))
		(else
			(set-tree-right
				tree
				(delete-tree (get-tree-right tree) target comp equal)))))

(define
	(insert-avl-tree tree target comp)
	(balance-avl
		(update-height
			(cond
				((tree-null? tree) (make-avl-node target empty-tree empty-tree 0))
				((comp target (get-tree-data tree))
					(set-avl-left
						tree
						(insert-avl-tree (get-tree-left tree) target comp)))
				(else
					(set-avl-right
						tree
						(insert-avl-tree (get-tree-right tree) target comp)))))))

(define
	(insert-avl-subtree tree target comp)
	(balance-avl
		(update-height
			(cond
				((tree-null? tree) target)
				((comp (get-tree-data target) (get-tree-data tree))
					(set-avl-left
						tree
						(insert-avl-tree (get-tree-left tree) target comp)))
				(else
					(set-avl-right
						tree
						(insert-avl-tree (get-tree-right tree) target comp)))))))

(define
	(delete-avl-tree tree target comp equal)
	(balance-avl
		(update-height
			(cond
				((tree-null? tree) empty-tree)
				((equal target (get-tree-data tree))
					(update-height
						(if
							(tree-null? (get-tree-right tree))
							(get-tree-left tree)
							(make-avl-node
								(get-tree-data
									(get-tree-right tree))
								(insert-avl-subtree
									(get-tree-left
										(get-tree-right tree))
									(get-tree-left tree)
									comp)
								(get-tree-right
									(get-tree-right tree))
								0))))
				((comp target (get-tree-data tree))
					(set-tree-left
						tree
						(delete-tree (get-tree-left tree) target comp equal)))
				(else
					(set-tree-right
						tree
						(delete-tree (get-tree-right tree) target comp equal)))))))

(define
	(pre-order tree)
	(if
		(tree-null? tree)
		'()
		(append
			(cons (get-tree-data tree) '())
			(pre-order (get-tree-left tree))
			(pre-order (get-tree-right tree)))))
			
(define
	(in-order tree)
	(if
		(tree-null? tree)
		'()
		(append
			(in-order (get-tree-left tree))
			(cons (get-tree-data tree) '())
			(in-order (get-tree-right tree)))))

(define
	(post-order tree)
	(if
		(tree-null? tree)
		'()
		(append
			(post-order (get-tree-left tree))
			(post-order (get-tree-right tree))
			(cons (get-tree-data tree) '()))))